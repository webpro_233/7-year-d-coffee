import http from "./axios";
import type AddPromotion from "@/types/Promotion";

function getPromotion() {
  return http.get("/promotion");
}
function savePromotion(promotion: AddPromotion) {
  return http.post("/ptomotion", promotion);
}

export default { getPromotion, savePromotion };
