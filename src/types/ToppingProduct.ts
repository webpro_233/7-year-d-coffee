export default interface ToppingProduct {
  size?: string;
  topping?: [];
  type?: string;
  total?: number;
  sweet?: string;
}
