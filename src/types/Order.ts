import type Customer from "./Customer";
import type { OrderItem } from "@/types/OrderItem";

export interface Order {
  id?: number;
  customerId?: number;
  discount: number;
  total: number;
  recieved: number;
  change: number;
  payment: string;
  customer?: Customer;
  orderItems?: OrderItem[];
  createdDate?: Date;
  updatedDate?: Date;
  deletedDate?: Date;
}
