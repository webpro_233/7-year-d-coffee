import type Product from "@/types/Product";
import type ToppingProduct from "@/types/ToppingProduct";
export interface OrderItem {
  name: string;
  amount: number;
  productId: number;
  total: number;
  price: number;
  createdDate?: Date;
  updatedDate?: Date;
  deletedDate?: Date;
  image?: string;
}
