export default interface AddPromotion {
  id?: number;
  name: string;
  discount: number;
}
