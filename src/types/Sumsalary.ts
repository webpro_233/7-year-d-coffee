import type { CheckInOut } from "./CheckInOut";
import type Employee from "@/types/employee";

export interface SummarySalary {
  id?: number;
  ss_date?: Date;
  hour?: number;
  salary?: number;
  checkInOut?: CheckInOut[];
  paid?: boolean;
  createdAt?: Date;

  updatedAt?: Date;

  deletedAt?: Date;
  employee?: Employee;
}
