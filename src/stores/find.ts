import { ref } from "vue";
import { defineStore } from "pinia";

export const useFindeStore = defineStore("find", () => {
  const dialog = ref(false);

  return { dialog };
});
