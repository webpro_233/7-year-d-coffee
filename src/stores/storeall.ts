import { defineStore } from "pinia";
import { ref } from "vue";
import type Product from "@/types/Product";
import type { OrderItem } from "@/types/OrderItem";
import { useLoadingStore } from "./loading";
import orderService from "@/services/order";
import { useMessageStore } from "./message";
import type { Order } from "@/types/Order";
import { useCustomerStore } from "@/stores/customer";
import { useOrderStore } from "@/stores/order";

export const usePointOfSale = defineStore("point of sale", () => {
  const messageStore = useMessageStore();
  const orderStore = useOrderStore();
  const dialogPayment = ref(false);
  const dialogPrompypay = ref(false);
  const customerStore = useCustomerStore();
  const dialogPromotion = ref(false);
  const realCode = ref("");
  const codePoint = ref();
  const orderItemList = ref<OrderItem[]>([]);
  const dialogTopping = ref(false);
  const toggle = ref(null);
  const dialogCheckPromotion = ref(false);
  const toggle2 = ref(null);
  const amenities = ref([]);
  const loadingStore = useLoadingStore();
  const topping = ref(0);
  const temProduct = ref<Product>({
    name: "",
    catagoryId: 1,
    category: "",
    type: "",
    size: "-",
    price: 0,
    image: "no_image.jpg",
    files: [],
  });
  const order = ref<Order>({
    customerId: 0,
    discount: 10,
    total: 0,
    recieved: 0,
    change: 0,
    payment: "promptpay",
    orderItems: orderItemList.value,
  });
  const promo = ref([
    {
      id: 1,
      name: "Member",
      price: 10,
      point: 30,
      code: "member001",
    },
    {
      id: 2,
      name: "3 แก้วลด 10",
      price: 10,
      point: 100,
      code: "discount03",
    },
  ]);
  const pointofsaleStore = usePointOfSale();
  const dialogComplteOrder = ref(false);
  const total_ = ref(0);
  const total_discount = ref(0);
  const totalAndDicount = ref(0);
  const recive_mon = ref(0);
  const change_money = ref(0);
  const CaltotalPrice = () => {
    if (pointofsaleStore.orderItemList.length > 0) {
      total_.value =
        orderItemList.value.reduce(
          (accumulator, currentValue) => accumulator + currentValue.total,
          0
        ) + topping.value;
      if (total_.value - total_discount.value <= 0) {
        totalAndDicount.value = 0;
      } else {
        totalAndDicount.value = total_.value - total_discount.value;
      }
      change_money.value = recive_mon.value - totalAndDicount.value;
      if (recive_mon.value <= 0) {
        change_money.value = 0;
      }
      if (recive_mon.value > 0) {
        if (change_money.value < 0) {
          messageStore.showError(
            `Money not enough : ${change_money.value} Bath`
          );
        }
      }

      return { total_, totalAndDicount, change_money };
    } else {
      total_.value = 0;
      return { total_ };
    }
  };
  const CalDiscout = () => {
    if (pointofsaleStore.orderItemList.length > 0) {
      (total_discount.value = total_discount.value + order.value.discount), 0;
      return { total_discount };
    } else {
      total_discount.value = 0;
      return { total_discount };
    }
  };
  const calMonAndDiscount = () => {
    if (pointofsaleStore.orderItemList.length > 0) {
      totalAndDicount.value = total_.value - total_discount.value;
    }
    if (recive_mon.value > 0) {
      if (recive_mon.value - totalAndDicount.value >= 0) {
        change_money.value = recive_mon.value - totalAndDicount.value;
      } else {
        change_money.value = 0;
      }
    }
    return { totalAndDicount };
  };
  const deleteAllOrder = async () => {
    orderItemList.value = [];
  };

  const addToOrder = (orderItem: OrderItem) => {
    orderItemList.value.push(orderItem);
  };

  const updatetmpProduct = (product: Product) => {
    temProduct.value = product;
    toggle.value = null;
    toggle2.value = null;
    amenities.value = [];
    return temProduct.value;
  };

  async function openOrder() {
    loadingStore.isLoading = true;
    try {
      if (order.value.orderItems?.length === 0) {
        messageStore.showError("You must select menu items");
        loadingStore.isLoading = false;
        total_.value = 0;
        total_discount.value = 0;
        totalAndDicount.value = 0;
        recive_mon.value = 0;
        change_money.value = 0;
        messageStore.showError(
          "Unable to save data due to incomplete data entry."
        );
        loadingStore.isLoading = false;

        return false;
      }
      if (
        order.value.change < 0 ||
        order.value.payment === "" ||
        !order.value.orderItems
      ) {
        messageStore.showError(
          "Unable to save data due to incomplete data entry."
        );
        loadingStore.isLoading = false;

        return false;
      }

      if (order.value.payment === "promptpay") {
        if (customerStore.customerId === "") {
          customerStore.customerId = 0 + "";
        }

        order.value = {
          customerId: customerStore.customerId,
          discount: total_discount.value,
          total: totalAndDicount.value,
          recieved: totalAndDicount.value,
          change: 0,
          payment: "cash",
          orderItems: orderItemList.value,
        };

        console.log(JSON.stringify(order.value));
      } else {
        if (change_money.value < 0 || recive_mon.value <= 0) {
          messageStore.showError(
            "Unable to save data due to incomplete data entry."
          );
          loadingStore.isLoading = false;

          return false;
        }
        if (customerStore.customerId === "") {
          customerStore.customerId = "0";
        }
        order.value = {
          customerId: customerStore.customerId,
          discount: total_discount.value,
          total: totalAndDicount.value,
          recieved: recive_mon.value,
          change: change_money.value,
          payment: "cash",
          orderItems: orderItemList.value,
        };

        console.log(JSON.stringify(order.value));
      }

      const res = await orderService.saveOrder(order.value);
      orderStore.tempOrder = res.data;
      dialogComplteOrder.value = true;

      order.value = {
        customerId: "",
        discount: 10,
        total: 0,
        recieved: 0,
        change: 0,
        payment: "promptpay",
        orderItems: orderItemList.value,
      };
      total_.value = 0;
      total_discount.value = 0;
      totalAndDicount.value = 0;
      recive_mon.value = 0;
      change_money.value = 0;
      orderItemList.value = [];
    } catch (e) {
      console.log(e);
      messageStore.showError("Cannot save orders");
    }
    loadingStore.isLoading = false;
  }
  const selectCode = (idCode: number) => {
    const correctCode = promo.value.findIndex((code) => code.id === idCode);
    realCode.value = promo.value[correctCode].code;
    order.value.discount = promo.value[correctCode].price;
    codePoint.value = promo.value[correctCode].point;
  };
  const checkCode = (Code: string) => {
    if (realCode.value === Code) {
      total_discount.value = order.value.discount;
      dialogPromotion.value = false;
      const customer = customerStore.customers.findIndex(
        (customer) => customer.id === order.value.customerId
      );
      let cusPoint = customerStore.customers[customer].point;
      if (cusPoint >= codePoint.value) {
        cusPoint = cusPoint - codePoint.value;
        customerStore.customers[customer].point = cusPoint;
        customerStore.customerService.updateCustomer(
          order.value.customerId + "",
          {
            ...customerStore.customers[customer],
            files: [],
          }
        );
      } else if (cusPoint < codePoint.value) {
        messageStore.showError(
          "Unable to save promotion data due to insufficient points."
        );
      }
    } else {
      messageStore.showError("Cannot save promotion");
    }
  };

  return {
    total_,
    total_discount,
    CaltotalPrice,
    CalDiscout,
    recive_mon,
    totalAndDicount,
    change_money,
    calMonAndDiscount,
    updatetmpProduct,
    dialogCheckPromotion,
    temProduct,
    dialogTopping,
    dialogPayment,
    dialogPromotion,
    dialogPrompypay,
    orderItemList,
    addToOrder,
    order,
    toggle,
    toggle2,
    amenities,
    openOrder,
    dialogComplteOrder,
    deleteAllOrder,
    promo,
    selectCode,
    checkCode,
    codePoint,
    topping,
  };
});
