import { ref } from "vue";
import { defineStore } from "pinia";

export const usePromotionStore = defineStore("promotion", () => {
  const dialog = ref(false);

  return { dialog };
});
