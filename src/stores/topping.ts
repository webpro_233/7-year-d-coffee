import { ref } from "vue";
import { defineStore } from "pinia";

export const useToppingStore = defineStore("topping", () => {
  const dialog = ref(false);

  return { dialog };
});
