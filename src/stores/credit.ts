import { ref } from "vue";
import { defineStore } from "pinia";

export const useCreditCardStore = defineStore("credit", () => {
  const creditCard = ref(false);

  return { creditCard };
});
