import { ref, computed } from "vue";
import { defineStore } from "pinia";
import { useUserStore } from "./user";
import type { VForm } from "vuetify/components";
import { useMessageStore } from "./message";
import auth from "@/services/auth";
import { useLoadingStore } from "./loading";
import router from "@/router";
import type User from "@/types/User";
const form = ref<InstanceType<typeof VForm> | null>(null);
export const useAuthStore = defineStore("auth", () => {
  const isLogin = () => {
    const user = localStorage.getItem("user");
    if (user) {
      return true;
    }
    return false;
  };
  const authName = ref("");

  // const getUser = () => {
  //   const userString = localStorage.getItem("user");
  //   if (!userString) return null;
  //   const user = JSON.parse(userString ?? "");
  //   return user;
  // };

  const loadingStore = useLoadingStore();
  const userStore = useUserStore();
  const messageStore = useMessageStore();
  const login = async (username: string, password: string) => {
    loadingStore.isLoading = true;
    try {
      const res = await auth.login(username, password);
      localStorage.setItem("user", JSON.stringify(res.data.user));
      localStorage.setItem("token", res.data.access_token);
      localStorage.setItem("employee", JSON.stringify(res.data.user.employee));
      router.push("/");
    } catch (e) {
      messageStore.showError("Username หรือ Password ไม่ถูกต้อง");
    }
    loadingStore.isLoading = false;
  };
  const logout = () => {
    localStorage.removeItem("user");
    localStorage.removeItem("token");
    localStorage.removeItem("employee");
    authName.value = "";
    router.replace("login");
  };

  return { login, logout, isLogin, authName };
});
