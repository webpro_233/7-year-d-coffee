import { ref } from "vue";
import { defineStore } from "pinia";

export const useQrcodeStore = defineStore("qrcode", () => {
  const dialog = ref(false);

  return { dialog };
});
