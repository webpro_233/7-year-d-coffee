import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";
import LoginView from "@/views/LoginView.vue";
import type User from "@/types/User";
import { ref } from "vue";
const user = ref<any | null>(localStorage.getItem("user"));
const user_ = JSON.parse(user.value);
const Autorized = () => {
  if (user_.role.toLowerCase() === "employee") {
    return router.push("/pageNotFound");
  } else {
    return true;
  }
};
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "dashboard",
      components: {
        default: () => HomeView,
        menu: () => import("@/components/menus/MainMenu.vue"),
      },
      meta: {
        layout: "FullLayout",
        requiresAuth: true,
      },
    },

    {
      path: "/login",
      name: "login",

      components: {
        default: LoginView,
      },
      meta: {
        layout: "FullLayout",
      },
    },
    {
      path: "/pointofsell",
      name: "pointheader",

      components: {
        // default: () => import("@/views/POS.vue"),
        default: () => import("@/components/POSofSell.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
      },
      meta: {
        layout: "FullLayout",
      },
    },
    {
      path: "/manager/employees",
      name: "Manager",

      components: {
        default: () => import("@/views/employees/EmployeeLoginOld.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
      },
      meta: {
        layout: "FullLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/prodlist",
      name: "prodlist",

      components: {
        default: () => import("../views/products/ProductList.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
      },
      meta: {
        layout: "FullLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/product-page",
      name: "product-page",

      components: {
        default: () => import("../views/products/ProductList.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
      },
      meta: {
        layout: "FullLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/matlist",
      name: "matlist",

      components: {
        default: () => import("../views/materials/MaterialView.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
      },
      meta: {
        layout: "FullLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/userlist",
      name: "userlist",

      components: {
        default: () => import("../views/users/UserList.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
      },
      meta: {
        layout: "FullLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/cuslist",
      name: "cuslist",

      components: {
        default: () => import("../views/customers/CustomerView.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
      },
      meta: {
        layout: "FullLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/histolist",
      name: "histolist",

      components: {
        default: () => import("../views/orders/OrderView.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
      },
      meta: {
        layout: "FullLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/employlist",
      name: "employlist",

      components: {
        default: () => import("../views/employees/EmployeeView.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
      },
      meta: {
        layout: "FullLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/bill",
      name: "bill",

      components: {
        default: () => import("@/components/POS/BillMember.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
      },
      meta: {
        layout: "FullLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/Payment",
      name: "Payment",

      components: {
        default: () => import("@/components/POS/PaymentOption.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
      },
      meta: {
        layout: "FullLayout",
        requiresAuth: true,
      },
    },

    {
      path: "/showBillById/:id",
      name: "showBillById",

      components: {
        default: () => import("../views/materials/ShowBillByIdView.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
      },
      meta: {
        layout: "FullLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/checkmaterial/:id",
      name: "checkmaterials",

      components: {
        default: () => import("../views/materials/CheckMaterialView.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
      },
      meta: {
        layout: "FullLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/material/showBills",
      name: "showBills",

      components: {
        default: () => import("../views/materials/ShowBillView.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
      },
      meta: {
        layout: "FullLayout",
      },
    },
    {
      path: "/order/:id",
      name: "order",

      components: {
        default: () => import("@/views/orders/OrderDetail.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
      },
      meta: {
        layout: "FullLayout",
      },
    },
    {
      path: "/GroupMember",
      name: "GM",

      components: {
        default: () => import("@/components/Home/GroupMember.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
      },
      meta: {
        layout: "FullLayout",
      },
    },
  ],
});
function isLogin() {
  const user = localStorage.getItem("user");
  if (user) {
    return true;
  }
  return false;
}
router.beforeEach((to, from) => {
  // instead of having to check every route record with
  // to.matched.some(record => record.meta.requiresAuth)
  if (to.meta.requiresAuth && !isLogin()) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    return {
      path: "/login",
      // save the location we were at to come back later
      query: { redirect: to.fullPath },
    };
  }
});
export default router;
